package de.klauss.wein.klausapp.data;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import de.klauss.wein.klausapp.R;

public class DataAdapterWines extends ArrayAdapter<DataHolder> {
    public DataAdapterWines(Context context, List<DataHolder> users) {
        super(context, 0, users);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        DataHolder data = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_dataholder, parent, false);
        }
        TextView label = (TextView) convertView.findViewById(R.id.data_label);
        TextView total = (TextView) convertView.findViewById(R.id.data_total);
        label.setText(data.label);
        total.setText(data.total);
        return convertView;
    }

}
