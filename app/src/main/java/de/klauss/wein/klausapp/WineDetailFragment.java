package de.klauss.wein.klausapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.io.Serializable;
import java.util.List;

import de.klauss.wein.klausapp.data.DataAdapterEditable;
import de.klauss.wein.klausapp.data.DataHolder;
import de.klauss.wein.klausapp.file.MenuDefinitionFileHandler;

import static android.app.Activity.RESULT_OK;

public class WineDetailFragment extends Fragment {
    public static final String BUNDLE_KEY = "TMP";
    private String title;
    private ListView listView;
    private List<DataHolder> detailsData;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        Intent intent = getActivity().getIntent();
        DataHolder data = (DataHolder)intent.getSerializableExtra(Intent.EXTRA_TEXT);
        title = intent.getStringExtra(Intent.EXTRA_HTML_TEXT);
        getActivity().setTitle(title);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(BUNDLE_KEY, new DataSerializier(detailsData));
        super.onSaveInstanceState(outState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (savedInstanceState != null && savedInstanceState.containsKey(BUNDLE_KEY)) {
            detailsData = ((DataSerializier) savedInstanceState.get(BUNDLE_KEY)).getDetailsData();
        } else {
            detailsData = MenuDefinitionFileHandler.getInstance().getSubItems(title);
        }

        DataAdapterEditable adapter = new DataAdapterEditable(getActivity(), detailsData);
        View rootView = inflater.inflate(R.layout.wine_detail_fragment, container, false);
        listView = (ListView) rootView.findViewById(R.id.wines_details_list);

        listView.setAdapter(adapter);

        Button finish = (Button)rootView.findViewById(R.id.details_finish);
        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra(WinesFragment.RESULT, new DataHolder(title, getTotal()));
                getActivity().setResult(RESULT_OK, intent);
                getActivity().finish();
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                TextView tmp = (TextView)view.findViewById(R.id.data_total_editable);
                String currentValue = tmp.getText().toString();
                new InputDialog().show(WineDetailFragment.this, listView, position, currentValue);
            }
        });

        return rootView;
    }

    private String getTotal() {
        int sum = 0;
        ListAdapter adapter = listView.getAdapter();
        for (int i = 0; i < listView.getAdapter().getCount(); i++) {
            DataHolder data = (DataHolder) adapter.getItem(i);
            if (data.total != null && !data.total.trim().isEmpty()) {
                sum += Integer.parseInt(data.total);
            }
        }
        return ""+sum;
    }

    class DataSerializier implements Serializable{
        private List<DataHolder> detailsData;
        public DataSerializier(List<DataHolder> detailsData) {
            this.detailsData = detailsData;
        }

        public List<DataHolder> getDetailsData() {
            return detailsData;
        }
    }
}
