package de.klauss.wein.klausapp.data;

import java.io.Serializable;

public class DataHolder implements Serializable {

    public String label;
    public String total;

    public DataHolder(String label, String total) {
        this.label = label;
        this.total = total;
    }
}
