package de.klauss.wein.klausapp.data;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import de.klauss.wein.klausapp.R;

public class DataAdapterEditable extends ArrayAdapter<DataHolder> {
    public DataAdapterEditable(Context context, List<DataHolder> users) {
        super(context, 0, users);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        DataHolder data = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_dataholder_editable, parent, false);
        }
        TextView label = (TextView) convertView.findViewById(R.id.data_label_editable);
        TextView total = (TextView) convertView.findViewById(R.id.data_total_editable);
        label.setText(data.label);
        total.setText(data.total);
        return convertView;
    }
}
