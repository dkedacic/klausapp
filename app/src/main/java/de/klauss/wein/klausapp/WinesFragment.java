package de.klauss.wein.klausapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

import de.klauss.wein.klausapp.data.DataAdapterWines;
import de.klauss.wein.klausapp.data.DataHolder;
import de.klauss.wein.klausapp.file.MenuDefinitionFileHandler;

import static android.app.Activity.RESULT_OK;

public class WinesFragment extends Fragment {
    public static final String RESULT = "result";
    private static List<DataHolder> winesData = null;
    private static DataAdapterWines adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        MenuDefinitionFileHandler fileHandler = MenuDefinitionFileHandler.getInstance();
        fileHandler.load( getActivity());
        if (winesData == null) {
            winesData = fileHandler.getMainMenus();
        }

        adapter = new DataAdapterWines(getActivity(), winesData);
        View rootView = inflater.inflate(R.layout.wines_fragment, container, false);
        ListView listView = (ListView) rootView.findViewById(R.id.wines_list);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                DataHolder data = (DataHolder) adapterView.getItemAtPosition(position);
                Intent intent = new Intent(getActivity(), WineDetailsActivity.class);
                intent.putExtra(Intent.EXTRA_TEXT, data);
                intent.putExtra(Intent.EXTRA_HTML_TEXT, data.label);
                startActivityForResult(intent, 0);
            }
        });
        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                DataHolder result = (DataHolder) data.getSerializableExtra(RESULT);
                for (DataHolder item :winesData) {
                    if (item.label.equals(result.label)) {
                        item.total = result.total;
                        adapter.notifyDataSetChanged();
                    }
                }
            }
        }
    }


}
