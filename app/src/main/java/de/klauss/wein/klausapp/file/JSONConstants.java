package de.klauss.wein.klausapp.file;

/**
 * Created by dkedacic on 16.03.2017.
 */

interface JSONConstants {

    String WINES = "wines";
    String SORT = "sort";
    String TOTAL = "total";
    String PROPERTIES = "properties";
}
