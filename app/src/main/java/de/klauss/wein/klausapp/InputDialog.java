package de.klauss.wein.klausapp;

import android.app.Dialog;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import de.klauss.wein.klausapp.data.DataAdapterEditable;
import de.klauss.wein.klausapp.data.DataHolder;

public class InputDialog {

    public void show(WineDetailFragment fragment, final ListView listView, final int position, String currentValue) {
        final Dialog dialog = new Dialog(fragment.getActivity());
        dialog.setContentView(R.layout.custom);
        dialog.setTitle("New value");

        final EditText editText = (EditText) dialog.findViewById(R.id.textEdit);
        editText.setText(currentValue);

        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String newText = editText.getText().toString();
                DataHolder data = (DataHolder) listView.getAdapter().getItem(position);
                if (newText.isEmpty() || newText.trim().isEmpty()) {
                    newText = "0";
                }
                data.total = newText;
                ((DataAdapterEditable)listView.getAdapter()).notifyDataSetChanged();
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}
