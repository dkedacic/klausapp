package de.klauss.wein.klausapp.file;

import android.support.v4.app.FragmentActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import de.klauss.wein.klausapp.R;
import de.klauss.wein.klausapp.data.DataHolder;

public class MenuDefinitionFileHandler implements JSONConstants{

    private JSONObject fullFileContent;

    private static final MenuDefinitionFileHandler INSTANCE = new MenuDefinitionFileHandler();

    public static MenuDefinitionFileHandler getInstance() {
        return INSTANCE;
    }

    public void load(FragmentActivity mainActivity) {
        String line = null;
        StringBuilder sb= new StringBuilder();
        try (InputStream is = mainActivity.getResources().openRawResource(R.raw.menu_items_definition); BufferedReader br = new BufferedReader(new InputStreamReader(is));) {
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            fullFileContent = new JSONObject(sb.toString());
        } catch (IOException | JSONException e) {
            throw new RuntimeException(e);
        }
    }

    public List<DataHolder> getMainMenus() {
        List<DataHolder> result = new ArrayList<>();
        try {
            JSONArray wines = fullFileContent.getJSONArray(WINES);
            for (int i = 0; i < wines.length(); i++) {
               JSONObject wine = (JSONObject)wines.get(i);
                result.add(new DataHolder(wine.getString(SORT), wine.getString(TOTAL)));
            }
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    public List<DataHolder> getSubItems(String wineSort) {
        List<DataHolder> result = new ArrayList<>();
        try {
            JSONArray wines = fullFileContent.getJSONArray(WINES);
            for (int i = 0; i < wines.length(); i++) {
                JSONObject wine = (JSONObject)wines.get(i);
                if (wine.getString(SORT).equals(wineSort)) {
                    JSONObject properties = (JSONObject)wine.get(PROPERTIES);
                    Iterator<String> iter = properties.keys();
                    while (iter.hasNext()) {
                        String key = iter.next();
                        String value = properties.getString(key);
                        result.add(new DataHolder(key, value));
                    }
                }
            }
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        return result;
    }


    public static String getString(JSONArray array, String q, int idx) {
        try {
            return ((JSONObject)array.get(idx)).getString(q);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static JSONObject getByIdx(JSONArray fullJson, int idx) {
        try {
            return (JSONObject) fullJson.get(idx);
        }catch ( JSONException ex) {
            throw new RuntimeException(ex);
        }
    }


}
